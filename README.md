# Запуск обучения и батч-предсказания

```
docker build -t best_push_time:dev -f Dockerfile . 
docker run -v /path/to/data:/srv/data best_push_time:dev "python3.8" /srv/src/train.py
```

# Запуск локального web-интерфейса

```
docker build -t best_push_time:app -f Dockerfile.app . 
docker run -v /path/to/data:/srv/data -p 5000:5000 best_push_time:app
```