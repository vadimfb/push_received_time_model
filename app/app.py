import pandas as pd
import numpy as np
from flask import Flask, request, render_template
import joblib

model = joblib.load('data/model.pkl')

CONTENT_TYPE = 'pic'
CONTENT_LIFETIME = 14


def predict(model, content_lifetime, content_type):
    hours = list(range(0, 24))
    df = pd.DataFrame({
        'user_id': [0] * 24,
        'push_hour': hours,
        'content_lifetime': [content_lifetime] * 24,
        'content_type': [content_type] * 24
    })
    preds = model.predict_proba(df)[:, 1]
    idx = np.argmax(preds)
    return hours[idx]


app = Flask(__name__)


@app.route("/", methods=["GET", "POST"])
def index():
    if request.method == 'POST':
        content_type = request.form["content_type"]
        if content_type == '':
            return render_template(
                "index.html",
                prediction='Empty strings are not used', content_type='')
        content_lifetime = request.form["content_lifetime"]
        if content_lifetime == '':
            return render_template(
                "index.html",
                prediction='Empty strings are not used', content_lifetime='')
        best_hour = predict(model, int(content_lifetime), content_type)
        return render_template("index.html",
                               best_hour=best_hour)
    else:
        return render_template("index.html")
