import datetime

TRAIN_DATE = datetime.datetime(2021, 12, 12).date()
VAL_DATE = datetime.datetime(2021, 12, 13).date()
TEST_DATE = datetime.datetime(2021, 12, 14).date()