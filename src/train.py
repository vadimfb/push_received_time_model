from typing import List, Tuple, Dict
from collections import namedtuple
import argparse
import yaml
import warnings
import pandas as pd

from sklearn.linear_model import LogisticRegression
from sklearn.metrics import roc_auc_score
from sklearn.pipeline import Pipeline
from sklearn.preprocessing import StandardScaler, OneHotEncoder
from sklearn.compose import ColumnTransformer
import joblib

from consts import TRAIN_DATE, VAL_DATE, TEST_DATE


Pool_type = namedtuple('Pool', ['features', 'target'])


warnings.filterwarnings("ignore")


def read_data(path: str) -> pd.DataFrame:
    """The method to read data

    Args:
        path (str): path

    Returns:
        pd.DataFrame: dataframe

    """
    df = pd.read_csv(path, sep=';')
    df = df.drop_duplicates().reset_index(drop=True)
    return df


def train_val_test_split(df: pd.DataFrame) -> Tuple[pd.DataFrame, pd.DataFrame, pd.DataFrame]:
    """The method to split dataframe into train, validate and test sets

    Args:
        df (pd.DataFrame): dataset

    Returns:
        Tuple[pd.DataFrame, pd.DataFrame, pd.DataFrame]: train, validate and test datasets

    """
    dt_seria = pd.to_datetime(df.push_time)
    df_train = df[dt_seria.dt.date == TRAIN_DATE].reset_index(drop=True)
    df_val = df[dt_seria.dt.date == VAL_DATE].reset_index(drop=True)
    df_test = df[dt_seria.dt.date == TEST_DATE].reset_index(drop=True)

    return df_train, df_val, df_test


def get_features_and_target(df: pd.DataFrame) -> Pool_type:
    """The method is to generate features and to get target

    Args:
        df:

    Returns:
        Tuple[pd.DataFrame, pd.Series]: feature dataframe and target seria

    """
    df.loc[:, 'push_hour'] = pd.to_datetime(df['push_time']).dt.hour
    df.loc[:, 'content_lifetime'] = (pd.to_datetime(df['push_time']) - pd.to_datetime(df['create_at'])).dt.days

    target = df['push_opened']

    return Pool_type(df[['user_id', 'push_hour', 'content_lifetime', 'content_type']], target)


def get_pipeline(reg_value: float = 1.) -> Pipeline:
    """The method to get pipeline

    Args:
        reg_value (float): regularization param

    Returns:
        Pipeline: pipeline

    """
    encoder = OneHotEncoder(handle_unknown='ignore')
    scaler = StandardScaler()

    data_pipeline = ColumnTransformer([
        ('numerical', scaler, ['push_hour', 'content_lifetime']),
        ('categorical', encoder, ['content_type'])
    ])

    log_reg = LogisticRegression(C=reg_value, random_state=0)
    pipeline = Pipeline([
        ('data_pipeline', data_pipeline),
        ('model', log_reg)
    ])

    return pipeline


def search_best_reg_value(train_pool: Pool_type,
                          val_pool: Pool_type,
                          pipeline: Pipeline,
                          reg_values: List[float]) -> Tuple[float, List]:
    """The method to search best hyperparameters

    Args:
        train_pool (Pool_type): train pool
        val_pool (Pool_type): validate pool
        pipeline (Pipeline): feature and model pipeline
        reg_values (List[float]): searching space

    Returns:
        Tuple[float, List]: best parameter and search results

    """
    scores = []

    model_step_id = 0

    for idx, step in enumerate(pipeline.steps):
        if step[0] == 'model':
            model_step_id = idx
    for reg_value in reg_values:
        pipeline.steps[model_step_id][1].C = reg_value
        pipeline.fit(train_pool.features, train_pool.target)
        preds = pipeline.predict_proba(val_pool.features)[:, 1]
        score = roc_auc_score(val_pool.target, preds)
        scores.append((score, reg_value))

    _, best_value = max(scores, key=lambda x: x[0])

    return best_value, scores


def train(pool: Pool_type, pipeline: Pipeline):
    """The method to fit model

    Args:
        pool (Pool_type): train data
        pipeline (Pipeline): feature and model pipeline

    Returns:
        None

    """
    pipeline.fit(pool.features, pool.target)


def save_model(pipeline: Pipeline, path: str):
    """The method to save pipeline

    Args:
        pipeline (Pipeline): pipeline
        path (str): path

    Returns:
        None

    """
    joblib.dump(pipeline, path)


def predict_best_hour(features: pd.DataFrame, pipeline: Pipeline, path: str):
    """The method to predict best push hour

    Args:
        features (pd.DataFrame): feature dataframe
        pipeline (Pipeline): pipeline
        path (str): path

    Returns:
        None

    """
    preds = pipeline.predict_proba(features)[:, 1]
    features['pred'] = pd.Series(preds)

    best_hour = (
        features
        .sort_values(['user_id', 'pred'], ascending=[True, False])
        .groupby('user_id')['push_hour']
        .apply(lambda x: x.values[0])
        .reset_index()
        .rename(columns={'push_hour': 'best_push_hour'})
    )

    best_hour.to_csv(path, index=False)


def read_config(path: str) -> Dict:
    """The method to read config

    Args:
        path (str): path

    Returns:
        Dict: config

    """
    with open(path, 'r') as f:
        config = yaml.load(f, Loader=yaml.FullLoader)

    return config


def main(config_path: str):
    """Main pipeline

    Args:
        config_path (str): path to config

    Returns:
        None

    """

    config = read_config(config_path)
    df = read_data(config['path_to_data'])
    train_df, val_df, test_df = train_val_test_split(df)

    train_pool = get_features_and_target(train_df)
    val_pool = get_features_and_target(val_df)
    test_pool = get_features_and_target(test_df)

    pipeline = get_pipeline()

    best_reg_value, _ = search_best_reg_value(train_pool, val_pool, pipeline, reg_values=config['reg_values'])

    train_features = pd.concat([train_pool.features, val_pool.features], ignore_index=True)
    train_target = pd.concat([train_pool.target, val_pool.target], ignore_index=True)
    train_pool = Pool_type(train_features, train_target)

    pipeline = get_pipeline(best_reg_value)

    train(train_pool, pipeline)
    save_model(pipeline, config['path_to_model'])

    predict_best_hour(test_pool.features, pipeline, config['path_to_best_push_time'])


if __name__ == '__main__':
    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument('--config', default='config.yml')
    args = arg_parser.parse_args()

    main(args.config)
